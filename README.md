# Container Images for Video Codec SDK

This repo includes the source for building base container images for the [NVIDIA Video Codec SDK](https://developer.nvidia.com/nvidia-video-codec-sdk).

## Versions Supported

The build recipes include the following versions:

- Video Codec SDK: 11.0.10
- CUDA: 11.2.2

## Getting Started

Build files are provided for the following Linux distributions:

- Ubuntu 18.04: [(*ubuntu18.04/Dockerfile*)](https://gitlab.com/nvidia/videosdk/blob/master/ubuntu18.04/Dockerfile)

### Building Images

First, clone the repo:

```bash
$ git clone https://gitlab.com/nvidia/container-images/video-codec-sdk.git
```

Download the Video Codec SDK (`11.0.10`) archive from the NVIDIA Video Codec SDK [page](https://developer.nvidia.com/nvidia-video-codec-sdk/download) and 
place it in the image directory. Build the Docker container:

```bash
$ docker build --pull \
   -t nvcr.io/nvidia/video-codec-sdk:11.0.10 \
   --build-arg repository=nvcr.io/nvidia/cuda \
   --build-arg flavor=devel \
   --file Dockerfile .
```

Once the image is built, you can run the Docker container:

```bash
$ docker run --gpus all -it \
    nvcr.io/nvidia/video-codec-sdk:11.0.10 \
    bash
```

### Running Samples

After launching the container in the previous step, you can build and run the samples from the container. 

First, install the FFMpeg headers and libraries to allow the samples to build cleanly.

```bash
apt-get install -y \
   libavutil-dev libavcodec-dev \
   libavformat-dev libswresample-dev \
   pkg-config
```

Now, we proceed to build the `AppDec` sample and run it with a sample input video file.